package kz.chocofood.chocofood_delivery.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.chocofood.chocofood_delivery.MainApplication;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.AccountActivity;
import kz.chocofood.chocofood_delivery.activity.ActiveOrdersActivity;
import kz.chocofood.chocofood_delivery.activity.ArchiveOrdersActivity;
import kz.chocofood.chocofood_delivery.activity.LoginActivity;
import kz.chocofood.chocofood_delivery.activity.ScheduleActivity;
import kz.chocofood.chocofood_delivery.fragments.ActiveOrdersFragment;
import kz.chocofood.chocofood_delivery.helpers.DialogHelper;
import kz.chocofood.chocofood_delivery.helpers.SharedPrefHelper;

public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FragmentResult.OnFragmentResult{
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.navigation_view) NavigationView navigation_view;
    @BindView(R.id.drawer) DrawerLayout drawer;

    @Inject SharedPrefHelper mSharedPrefHelper;
    private boolean finishWithAnimation = false;
    ActionBarDrawerToggle toggle;

    public class MENU_ITEMS{
        public final static int DRAWER_ID_ACCOUNT = 0;
        public final static int DRAWER_ID_ACTIVE_ORDERS = 1;
        public final static int DRAWER_ID_ARCHIVE_ORDERS = 2;
        public final static int DRAWER_ID_SCHEDULE = 3;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
        ((MainApplication)this.getApplication()).getComponentNet().inject(this);
        setSupportActionBar(toolbar);
        initDrawerMenu();
        if (getCurrentFragment() == null) {
            displayFragment(onInitFragment());
        }
        //new DialogHelper().showNetworkDialog(this);
    }

    public NavigationView getNavigation_view(){
        return navigation_view;
    }

    public int getLayout(){
        return R.layout.activity_base;
    }

    protected abstract boolean isFinishWithAnimation();

    public void initDrawerMenu(){
        setupToggleListener();
        navigation_view.setNavigationItemSelectedListener(BaseActivity.this);
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStack();
        }
        else{
            super.onBackPressed();
        }
    }

    public  ActionBarDrawerToggle getToggle(){
        return toggle;
    }

    public void setupToggleListener(){
        drawer = (DrawerLayout)findViewById(R.id.drawer);
        toggle = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(item.isChecked()) item.setChecked(false);
        else item.setChecked(true);

        //Closing drawer on item click
        drawer.closeDrawers();
        //Check to see which item was being clicked and perform appropriate action
        switch (item.getItemId()){
            case R.id.menu_item_account:
                startActivity(AccountActivity.newIntent(getApplicationContext()));
                break;

            case R.id.menu_item_active_orders:
                startActivity(ActiveOrdersActivity.newIntent(getApplicationContext()));
                break;

            case R.id.menu_item_archive_orders:
                startActivity(ArchiveOrdersActivity.newIntent(getApplicationContext()));
                break;

            case R.id.menu_item_shedule:
                startActivity(ScheduleActivity.newIntent(getApplicationContext()));
                break;

            case R.id.menu_item_logout:
                logout();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected abstract boolean isEnabledDrawer();

    public void disableDrawer(boolean disable) {
        if (drawer != null) {
            if (disable) {
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            } else {
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            }
        }
    }

    public Toolbar getToolbar(){
        return toolbar;
    }

    protected abstract BaseFragment onInitFragment();

    protected abstract int onSelectedMenuId();

    protected void hideSoftWareKeyboard()
    {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getToolbar().getWindowToken(), 0);
    }


    public void displayFragment(BaseFragment fragment) {
        displayFragment(fragment, null);
    }

    public void displayFragment(BaseFragment fragment, Bundle args) {
        FragmentManager fm = getSupportFragmentManager();

        if (fm.getBackStackEntryCount() > 0 && getCurrentFragment() != null && getCurrentFragment().getFragmentTag().equals(fragment.getFragmentTag())) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(R.anim.from_right, R.anim.not_going, R.anim.not_going, R.anim.not_going);
            if (fm.getBackStackEntryCount() > 0) {
                ft.hide(getCurrentFragment());
            }

            BaseFragment existFragment = (BaseFragment) fm.findFragmentByTag(fragment.getFragmentTag());

            ft.show(existFragment);
            ft.commit();
            return;
        }

        if (args != null) {
            fragment.setArguments(args);

        }

        FragmentTransaction ft = fm.beginTransaction();

        if (fm.getBackStackEntryCount() > 0) {
            ft.hide(getCurrentFragment());
        }

        ft.add(R.id.fragment_container, fragment, fragment.getFragmentTag());
        ft.addToBackStack(fragment.getFragmentTag());

        ft.commit();

    }
    private BaseFragment getCurrentFragment() {
        FragmentManager fm = getSupportFragmentManager();
        int entryCount = fm.getBackStackEntryCount();
        if (entryCount > 0) {
            String fragmentTag = fm.getBackStackEntryAt(entryCount - 1).getName();
            return (BaseFragment) fm.findFragmentByTag(fragmentTag);
        }
        return null;
    }

    public void openDrawer() {
        drawer.openDrawer(Gravity.START);
        hideSoftWareKeyboard();
    }

    public void closeDrawer() {
        drawer.closeDrawer(Gravity.START);
    }
    
    private void popCurrentFragment() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStackImmediate();
        }
    }

    private void logout(){
        mSharedPrefHelper.clearUserData();
        startActivity(LoginActivity.newIntent(getApplicationContext()));
        finish();
    }
}
