package kz.chocofood.chocofood_delivery.base;

import android.os.Bundle;

/**
 * Created by User on 15.08.2017.
 */

public class FragmentResult {
    public static final int BACK_PRESSED = 0; ///< нажатие кнопки назад
    public static final int RETURN_RESULT = 1;
    public static final int RETURN_CITY = 2;
    public static final int RETURN_FILTERS = 3;
    public static final int RETURN_ADDRESS = 4;
    public static final int RETURN_PARTNER_SERVICE = 5;

    /**
     * \brief Интерфейс с наследуемом методом onFragmentResult
     */
    public interface OnFragmentResult {
        void onFragmentResult(int result, Bundle args, String tag);
    }
}