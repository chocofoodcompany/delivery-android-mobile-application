package kz.chocofood.chocofood_delivery.base;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.chocofood.chocofood_delivery.R;


public abstract class BaseFragment extends Fragment {

    public View baseview;
    View content;
    View loading;

    public abstract String getFragmentTag();

    protected abstract int onLayoutId();

    protected abstract void customizeActionBar();

    protected abstract  boolean isDrawerDisabled();

    protected void disableDrawer()
    {
        getMainActivity().disableDrawer(isDrawerDisabled());
    }

    public BaseActivity getMainActivity()
    {
        return (BaseActivity) getActivity();
    }

    public BaseFragment() {

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        baseview = inflater.inflate(onLayoutId(),container,false);
        return baseview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        customizeActionBar();
        disableDrawer();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            customizeActionBar();
            disableDrawer();
        }
    }

    public void onFragmentResult(int result, Bundle args)
    {

    }

    public void onFragmentResume(){
        getMainActivity().hideSoftWareKeyboard();
    }
}
