package kz.chocofood.chocofood_delivery.rest;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import dagger.Component;
import dagger.Provides;
import io.reactivex.Observable;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.models.ActiveOrder;
import kz.chocofood.chocofood_delivery.models.OrderStep;
import kz.chocofood.chocofood_delivery.models.User;
import kz.chocofood.chocofood_delivery.utils.Constants;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by User on 24.08.2017.
 */

public interface API {
    //Order calls
    @POST(RestConstants.ACCEPT_ORDER_API)
    Observable<Response<OrderStep>> acceptOrder(@Header(RestConstants.AUTHORIZATION_CONSTANT) String token, @Path("id") String order_id);

    @POST(RestConstants.RESTAURANT_STEP)
    Observable<Response<OrderStep>> restaurantOrder(@Header(RestConstants.AUTHORIZATION_CONSTANT) String token, @Path("id") String order_id);

    @POST(RestConstants.RESTAURANT_OUT_STEP)
    Observable<Response<OrderStep>> restaurantOutOrder(@Header(RestConstants.AUTHORIZATION_CONSTANT) String token, @Path("id") String order_id);

    @POST(RestConstants.CLIENT_STEP)
    Observable<Response<OrderStep>> clientOrder(@Header(RestConstants.AUTHORIZATION_CONSTANT) String token, @Path("id") String order_id);

    @POST(RestConstants.ORDER_DELIVERED_STEP)
    Observable<Response<OrderStep>> deliveredOrder(@Header(RestConstants.AUTHORIZATION_CONSTANT) String token, @Path("id") String order_id);



    @POST(RestConstants.POST_AUTH)
    Observable<Response<User>> getUserToken(@Body HashMap<String, String> user);

    @GET(RestConstants.GET_COURIER_STATUS)
    Observable<Response<User>> getUserStatus( @Header(RestConstants.AUTHORIZATION_CONSTANT) String token);

    @POST(RestConstants.POST_COURIER_READY)
    Observable<Response<Void>> applyUserStatus( @Header(RestConstants.AUTHORIZATION_CONSTANT) String token);

    @GET(RestConstants.GET_ACTIVE_ORDERS)
    Observable<Response<List<ActiveOrder>>> getActiveOrders(@Header(RestConstants.AUTHORIZATION_CONSTANT) String token);

    @FormUrlEncoded
    @POST(RestConstants.POST_FIREBASE_DEVICE_ID)
    Observable<Response<Void>> sendFirebaseToken(@Header(RestConstants.AUTHORIZATION_CONSTANT) String token, @Field("device_id") String device_id);

}
