package kz.chocofood.chocofood_delivery.rest;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by User on 31.08.2017.
 */

public class APIError {
    private int mErrorCode;
    @NonNull
    private List<String> username;
    @NonNull
    private List<String> password;
    @NonNull
    private List<String> non_field_errors;
    @NonNull
    private List<String> detail;

    public APIError(){}

    @NonNull
    public List<String> getUsername() {
        return username;
    }

    @NonNull
    public List<String> getPassword() {
        return password;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    @NonNull
    public List<String> getNon_field_errors() {
        return non_field_errors;
    }

    @NonNull
    public List<String> getDetail() {
        return detail;
    }
}
