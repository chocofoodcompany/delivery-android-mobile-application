package kz.chocofood.chocofood_delivery.rest;

/**
 * Created by User on 29.08.2017.
 */

public class RestConstants {
    // Order calls
    public static final String AUTHORIZATION_CONSTANT = "Authorization";
    public static final String HEADER_AUTH_TOKEN_CONSTANT = "Token ";

    public static final String ACCEPT_ORDER_API = "api/courier/order/{id}/accept/";
    public static final String RESTAURANT_STEP = "/api/courier/order/{id}/restaurant/";
    public static final String RESTAURANT_OUT_STEP = "/api/courier/order/{id}/restaurant_out/";
    public static final String CLIENT_STEP = "/api/courier/order/{id}/client/";
    public static final String ORDER_DELIVERED_STEP = "/api/courier/order/{id}/delivered/";

    public static final String POST_AUTH = "api/main/auth/";
    public static final String GET_COURIER_STATUS = "api/courier/process/status/";
    public static final String POST_COURIER_READY = "api/courier/process/ready/";
    public static final String GET_ACTIVE_ORDERS = "api/courier/order/active/";
    public static final String POST_FIREBASE_DEVICE_ID = "api/courier/process/device_id/";
}
