package kz.chocofood.chocofood_delivery.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import butterknife.BindView;

/**
 * Created by User on 24.08.2017.
 */

public class User {

    @SerializedName("username")
    String mUsername;

    @SerializedName("password")
    String mPassword;

    @SerializedName("token")
    String mToken;

    @SerializedName("is_working_now")
    Boolean is_working;

    @SerializedName("can_ready")
    Boolean can_ready;

    @SerializedName("is_ready")
    Boolean is_ready;

    @SerializedName("countdown")
    String countdown;

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public Boolean getIs_working() {
        return is_working;
    }

    public void setIs_working(Boolean is_working) {
        this.is_working = is_working;
    }

    public Boolean getCan_ready() {
        return can_ready;
    }

    public void setCan_ready(Boolean can_ready) {
        this.can_ready = can_ready;
    }

    public Boolean getIs_ready() {
        return is_ready;
    }

    public void setIs_ready(Boolean is_ready) {
        this.is_ready = is_ready;
    }

    public String getCountdown() {
        return countdown;
    }

    public void setCountdown(String countdown) {
        this.countdown = countdown;
    }
}
