package kz.chocofood.chocofood_delivery.models;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActiveOrder {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("latitude_from")
    @Expose
    private String latitudeFrom;
    @SerializedName("longitude_from")
    @Expose
    private String longitudeFrom;
    @SerializedName("latitude_to")
    @Expose
    private String latitudeTo;
    @SerializedName("longitude_to")
    @Expose
    private String longitudeTo;
    @SerializedName("status")
    @Expose
    private List<Object> status = null;
    @SerializedName("datetime_start")
    @Expose
    private Object datetimeStart;
    @SerializedName("datetime_end")
    @Expose
    private Object datetimeEnd;
    @SerializedName("order")
    @Expose
    private Order order;
    @SerializedName("courier")
    @Expose
    private List<Object> courier = null;
    @SerializedName("city")
    @Expose
    private List<String> city = null;
    @SerializedName("order_info")
    @Expose
    private OrderInfo orderInfo;
    @SerializedName("order_contact")
    @Expose
    private OrderContact orderContact;
    @SerializedName("hub")
    @Expose
    private Object hub;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLatitudeFrom() {
        return latitudeFrom;
    }

    public void setLatitudeFrom(String latitudeFrom) {
        this.latitudeFrom = latitudeFrom;
    }

    public String getLongitudeFrom() {
        return longitudeFrom;
    }

    public void setLongitudeFrom(String longitudeFrom) {
        this.longitudeFrom = longitudeFrom;
    }

    public String getLatitudeTo() {
        return latitudeTo;
    }

    public void setLatitudeTo(String latitudeTo) {
        this.latitudeTo = latitudeTo;
    }

    public String getLongitudeTo() {
        return longitudeTo;
    }

    public void setLongitudeTo(String longitudeTo) {
        this.longitudeTo = longitudeTo;
    }

    public List<Object> getStatus() {
        return status;
    }

    public void setStatus(List<Object> status) {
        this.status = status;
    }

    public Object getDatetimeStart() {
        return datetimeStart;
    }

    public void setDatetimeStart(Object datetimeStart) {
        this.datetimeStart = datetimeStart;
    }

    public Object getDatetimeEnd() {
        return datetimeEnd;
    }

    public void setDatetimeEnd(Object datetimeEnd) {
        this.datetimeEnd = datetimeEnd;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<Object> getCourier() {
        return courier;
    }

    public void setCourier(List<Object> courier) {
        this.courier = courier;
    }

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public OrderContact getOrderContact() {
        return orderContact;
    }

    public void setOrderContact(OrderContact orderContact) {
        this.orderContact = orderContact;
    }

    public Object getHub() {
        return hub;
    }

    public void setHub(Object hub) {
        this.hub = hub;
    }


    public class Order {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("amount_to_partner")
        @Expose
        private String amountToPartner;
        @SerializedName("amount_from_client")
        @Expose
        private String amountFromClient;
        @SerializedName("callback_url")
        @Expose
        private String callbackUrl;
        @SerializedName("address_from")
        @Expose
        private String addressFrom;
        @SerializedName("address_to")
        @Expose
        private String addressTo;
        @SerializedName("client_order_code")
        @Expose
        private String clientOrderCode;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getAmountToPartner() {
            return amountToPartner;
        }

        public void setAmountToPartner(String amountToPartner) {
            this.amountToPartner = amountToPartner;
        }

        public String getAmountFromClient() {
            return amountFromClient;
        }

        public void setAmountFromClient(String amountFromClient) {
            this.amountFromClient = amountFromClient;
        }

        public String getCallbackUrl() {
            return callbackUrl;
        }

        public void setCallbackUrl(String callbackUrl) {
            this.callbackUrl = callbackUrl;
        }

        public String getAddressFrom() {
            return addressFrom;
        }

        public void setAddressFrom(String addressFrom) {
            this.addressFrom = addressFrom;
        }

        public String getAddressTo() {
            return addressTo;
        }

        public void setAddressTo(String addressTo) {
            this.addressTo = addressTo;
        }

        public String getClientOrderCode() {
            return clientOrderCode;
        }

        public void setClientOrderCode(String clientOrderCode) {
            this.clientOrderCode = clientOrderCode;
        }

    }

    public class OrderContact {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("partner_name")
        @Expose
        private String partnerName;
        @SerializedName("partner_phone")
        @Expose
        private String partnerPhone;
        @SerializedName("client_phone")
        @Expose
        private String clientPhone;
        @SerializedName("client_name")
        @Expose
        private String clientName;
        @SerializedName("order")
        @Expose
        private Integer order;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPartnerName() {
            return partnerName;
        }

        public void setPartnerName(String partnerName) {
            this.partnerName = partnerName;
        }

        public String getPartnerPhone() {
            return partnerPhone;
        }

        public void setPartnerPhone(String partnerPhone) {
            this.partnerPhone = partnerPhone;
        }

        public String getClientPhone() {
            return clientPhone;
        }

        public void setClientPhone(String clientPhone) {
            this.clientPhone = clientPhone;
        }

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public Integer getOrder() {
            return order;
        }

        public void setOrder(Integer order) {
            this.order = order;
        }

    }

    public class OrderInfo {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("datetime_created")
        @Expose
        private String datetimeCreated;
        @SerializedName("datetime_partner")
        @Expose
        private String datetimePartner;
        @SerializedName("datetime_client")
        @Expose
        private String datetimeClient;
        @SerializedName("order")
        @Expose
        private Integer order;
        @SerializedName("client")
        @Expose
        private Integer client;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDatetimeCreated() {
            return datetimeCreated;
        }

        public void setDatetimeCreated(String datetimeCreated) {
            this.datetimeCreated = datetimeCreated;
        }

        public String getDatetimePartner() {
            return datetimePartner;
        }

        public void setDatetimePartner(String datetimePartner) {
            this.datetimePartner = datetimePartner;
        }

        public String getDatetimeClient() {
            return datetimeClient;
        }

        public void setDatetimeClient(String datetimeClient) {
            this.datetimeClient = datetimeClient;
        }

        public Integer getOrder() {
            return order;
        }

        public void setOrder(Integer order) {
            this.order = order;
        }

        public Integer getClient() {
            return client;
        }

        public void setClient(Integer client) {
            this.client = client;
        }

    }

}