package kz.chocofood.chocofood_delivery.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 02.09.2017.
 */

public class OrderStep {
    @SerializedName("current")
    @Expose
    String current;

    @SerializedName("display")
    @Expose
    String display;

    @SerializedName("next")
    @Expose
    String next;

    @SerializedName("next_text")
    @Expose
    String next_text;

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getNext_text() {
        return next_text;
    }

    public void setNext_text(String next_text) {
        this.next_text = next_text;
    }
}
