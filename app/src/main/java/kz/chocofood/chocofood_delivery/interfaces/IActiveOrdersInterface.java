package kz.chocofood.chocofood_delivery.interfaces;

import android.view.View;

/**
 * Created by User on 22.08.2017.
 */

public interface IActiveOrdersInterface {
    public void onViewClicked(View view, int position);
}
