package kz.chocofood.chocofood_delivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.BaseMenuPresenter;
import android.test.ActivityInstrumentationTestCase;
import android.util.Log;

import javax.inject.Inject;

import kz.chocofood.chocofood_delivery.MainApplication;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.fragments.LoginFragment;
import kz.chocofood.chocofood_delivery.rest.API;

public class LoginActivity extends BaseActivity{

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, LoginActivity.class);
    }

    @Override
    protected boolean isEnabledDrawer() {
        return true;
    }

    @Override
    protected BaseFragment onInitFragment() {
        return LoginFragment.newInstance();
    }

    @Override
    protected int onSelectedMenuId() {
        return -1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected boolean isFinishWithAnimation() {
        return true;
    }

    @Override
    public void onFragmentResult(int result, Bundle args, String tag) {

    }
}
