package kz.chocofood.chocofood_delivery.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import io.reactivex.annotations.NonNull;
import kz.chocofood.chocofood_delivery.BuildConfig;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.fragments.StartFragment;

public class StartActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected boolean isFinishWithAnimation() {
        return false;
    }

    @Override
    protected boolean isEnabledDrawer() {
        return false;
    }

    @Override
    protected BaseFragment onInitFragment() {
        return StartFragment.newInstance();
    }

    @Override
    protected int onSelectedMenuId() {
        return -1;
    }

    @Override
    public void onFragmentResult(int result, Bundle args, String tag) {

    }
}
