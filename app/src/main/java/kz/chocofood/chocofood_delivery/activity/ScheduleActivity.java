package kz.chocofood.chocofood_delivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.fragments.ScheduleFragment;

public class ScheduleActivity extends BaseActivity {

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, ScheduleActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected boolean isFinishWithAnimation() {
        return false;
    }

    @Override
    protected boolean isEnabledDrawer() {
        return true;
    }

    @Override
    protected BaseFragment onInitFragment() {
        return ScheduleFragment.newInstance();
    }

    @Override
    protected int onSelectedMenuId() {
        return MENU_ITEMS.DRAWER_ID_SCHEDULE;
    }

    @Override
    public void onFragmentResult(int result, Bundle args, String tag) {

    }
}
