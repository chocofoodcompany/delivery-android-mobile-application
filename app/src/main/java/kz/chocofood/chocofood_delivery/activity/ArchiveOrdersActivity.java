package kz.chocofood.chocofood_delivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.BaseAdapter;

import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.fragments.ActiveOrdersFragment;
import kz.chocofood.chocofood_delivery.fragments.ArchiveOrdersFragment;

public class ArchiveOrdersActivity extends BaseActivity {

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, ArchiveOrdersActivity.class);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected boolean isFinishWithAnimation() {
        return false;
    }

    @Override
    protected boolean isEnabledDrawer() {
        return true;
    }

    @Override
    protected BaseFragment onInitFragment() {
        return ArchiveOrdersFragment.newInstance();
    }

    @Override
    protected int onSelectedMenuId() {
        return MENU_ITEMS.DRAWER_ID_ARCHIVE_ORDERS;
    }

    @Override
    public void onFragmentResult(int result, Bundle args, String tag) {

    }
}
