package kz.chocofood.chocofood_delivery.di.modules;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by User on 24.08.2017.
 */

@Module
public class ModuleApp {

    Application mApplication;

    public ModuleApp(Application application){
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication(){
        return this.mApplication;
    }

    @Provides
    @Singleton
        // Application reference must come from AppModule.class
    SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }
}
