package kz.chocofood.chocofood_delivery.di.component;

import javax.inject.Singleton;

import dagger.Component;
import kz.chocofood.chocofood_delivery.MainApplication;
import kz.chocofood.chocofood_delivery.activity.LoginActivity;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.di.modules.ModuleApp;
import kz.chocofood.chocofood_delivery.di.modules.ModuleNet;
import kz.chocofood.chocofood_delivery.fragments.ActiveOrdersFragment;
import kz.chocofood.chocofood_delivery.fragments.LoginFragment;
import kz.chocofood.chocofood_delivery.fragments.RiderStatusFragment;
import kz.chocofood.chocofood_delivery.rest.ErrorUtils;
import retrofit2.Retrofit;

/**
 * Created by User on 24.08.2017.
 */

@Singleton
@Component(modules = {ModuleApp.class, ModuleNet.class})

public interface ComponentNet {
    void inject(MainApplication application);
    void inject(LoginFragment fragment);
    void inject(ErrorUtils errorClass);
    void inject(RiderStatusFragment riderFragment);
    void inject(ActiveOrdersFragment activeOrdersFragment);
    void inject(BaseActivity activity);
    Retrofit getRetrofit();
}
