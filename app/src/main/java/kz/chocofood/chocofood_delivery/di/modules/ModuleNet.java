package kz.chocofood.chocofood_delivery.di.modules;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.provider.SyncStateContract;
import android.widget.ProgressBar;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kz.chocofood.chocofood_delivery.BuildConfig;
import kz.chocofood.chocofood_delivery.helpers.SharedPrefHelper;
import kz.chocofood.chocofood_delivery.rest.API;
import kz.chocofood.chocofood_delivery.rest.ErrorUtils;
import kz.chocofood.chocofood_delivery.utils.Constants;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 24.08.2017.
 */
@Module
public class ModuleNet {
    public ModuleNet(){
    }

    @Provides
    @Singleton
    Cache providesOkHttpCache(Application application){
        int cacheSize = 50 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson providesGson(){
        return new GsonBuilder()
                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(Gson gson, OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(Cache cache){
        return new OkHttpClient.Builder()
                .connectTimeout(60000, TimeUnit.MILLISECONDS)
                .readTimeout(60000,TimeUnit.MILLISECONDS)
                .writeTimeout(60000,TimeUnit.MILLISECONDS)
                .addInterceptor(BuildConfig.DEBUG?
                        new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY):
                        new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE))
                .cache(cache)
                .build();
    }

    @Provides
    @Singleton
    API providesNetAdapter(Retrofit retrofit){
        return retrofit.create(API.class);
    }

}
