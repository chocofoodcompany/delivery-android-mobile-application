package kz.chocofood.chocofood_delivery.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.adapters.ActiveOrdersAdapter;
import kz.chocofood.chocofood_delivery.adapters.ArchiveOrdersAdapter;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.models.ActiveOrder;
import kz.chocofood.chocofood_delivery.models.ArchiveOrder;

public class ArchiveOrdersFragment extends BaseFragment {

    @BindView(R.id.archive_orders_list) RecyclerView mArchiveOrders;
    ArchiveOrdersAdapter mArchiveAdapter;
    LinearLayoutManager mArchiveManager;
    ArrayList<ArchiveOrder>  mArchives;
    @Override
    public String getFragmentTag() {
        return ArchiveOrdersFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_archive_orders;
    }

    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.fragment_archive_orders_title));
        toolbar.setBackgroundColor(getResources().getColor(R.color.choco_color_red));
        toolbar.setTitleTextColor(getResources().getColor(R.color.choco_color_white));
        getMainActivity().setSupportActionBar(toolbar);
        getMainActivity().getSupportActionBar().setDisplayShowHomeEnabled(true);
        getMainActivity().setupToggleListener();
        getMainActivity().getNavigation_view().getMenu().getItem(BaseActivity.MENU_ITEMS.DRAWER_ID_ARCHIVE_ORDERS).setChecked(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getMainActivity().openDrawer();
            }
        });
    }

    @Override
    protected boolean isDrawerDisabled() {
        return false;
    }

    public ArchiveOrdersFragment() {
        // Required empty public constructor
    }

    public static ArchiveOrdersFragment newInstance() {
        ArchiveOrdersFragment fragment = new ArchiveOrdersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this,baseview);
        setupAdapter();
        return baseview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setupAdapter(){
        mArchives = new ArrayList<ArchiveOrder>();
        mArchives.add(new ArchiveOrder());
        mArchives.add(new ArchiveOrder());
        mArchiveManager = new LinearLayoutManager(getContext());
        mArchiveAdapter = new ArchiveOrdersAdapter(getContext(),mArchives);
        mArchiveOrders.setLayoutManager(mArchiveManager);
        mArchiveOrders.setAdapter(mArchiveAdapter);
    }
}
