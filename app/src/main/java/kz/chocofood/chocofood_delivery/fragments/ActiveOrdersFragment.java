package kz.chocofood.chocofood_delivery.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kz.chocofood.chocofood_delivery.MainApplication;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.ActiveOrdersActivity;
import kz.chocofood.chocofood_delivery.adapters.ActiveOrdersAdapter;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.helpers.SharedPrefHelper;
import kz.chocofood.chocofood_delivery.interfaces.IActiveOrdersInterface;
import kz.chocofood.chocofood_delivery.models.ActiveOrder;
import kz.chocofood.chocofood_delivery.models.OrderStep;
import kz.chocofood.chocofood_delivery.rest.API;
import kz.chocofood.chocofood_delivery.rest.APIError;
import kz.chocofood.chocofood_delivery.rest.ErrorUtils;
import kz.chocofood.chocofood_delivery.rest.RestConstants;
import retrofit2.Retrofit;

public class ActiveOrdersFragment extends BaseFragment implements IActiveOrdersInterface{
    @BindView(R.id.active_orders_list) RecyclerView mActiveOrders;
    @Inject API api;
    @Inject Retrofit mRetrofit;
    @Inject SharedPrefHelper mSharedPrefHelper;

    ActiveOrdersAdapter mOrdersAdapter;
    LinearLayoutManager mOrdersManager;
    List<ActiveOrder> mOrders;

    @Override
    public String getFragmentTag() {
        return ActiveOrdersFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_active_orders;
    }

    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.fragment_active_orders_title));
        toolbar.setBackgroundColor(getResources().getColor(R.color.choco_color_red));
        toolbar.setTitleTextColor(getResources().getColor(R.color.choco_color_white));
        getMainActivity().setSupportActionBar(toolbar);
        getMainActivity().getSupportActionBar().setDisplayShowHomeEnabled(true);
        getMainActivity().setupToggleListener();
        getMainActivity().getNavigation_view().getMenu().getItem(BaseActivity.MENU_ITEMS.DRAWER_ID_ACTIVE_ORDERS).setChecked(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getMainActivity().openDrawer();
            }
        });
    }

    @Override
    protected boolean isDrawerDisabled() {
        return false;
    }

    public ActiveOrdersFragment() {
        // Required empty public constructor
    }

    public static ActiveOrdersFragment newInstance() {
        ActiveOrdersFragment fragment = new ActiveOrdersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, baseview);
        ((MainApplication)getActivity().getApplication()).getComponentNet().inject(this);
        mOrders = new ArrayList<ActiveOrder>();
        setupAdapter();
        getOrders();
        return baseview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setupAdapter(){
        mOrdersManager = new LinearLayoutManager(getContext());
        mOrdersAdapter = new ActiveOrdersAdapter(getContext(),mOrders,this);
        mActiveOrders.setLayoutManager(mOrdersManager);
        mActiveOrders.setAdapter(mOrdersAdapter);
    }

    @Override
    public void onViewClicked(View view, int position) {
        if(view.getId() == R.id.order_details_button){
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.from_right,R.anim.to_left)
                    .replace(R.id.fragment_container, new OrderDetailsFragment(), OrderDetailsFragment.class.getName())
                    .addToBackStack(null)
                    .commit();
        }
        if(view.getId() == R.id.order_make_step_button){
            Double d = (Double)mOrders.get(position).getStatus().get(0);
            if(Double.toString(d.intValue()).equals("201.0")){
                restaurantOrder(mOrders.get(position).getId(), position);
            }
            if(Double.toString(d.intValue()).equals("300.0")){
                restaurantOutOrder(mOrders.get(position).getId(), position);
            }
            if(Double.toString(d.intValue()).equals("301.0")){
                clientOrder(mOrders.get(position).getId(), position);
            }
            if(Double.toString(d.intValue()).equals("400.0")){
                deliveredOrder(mOrders.get(position).getId(), position);
            }
        }
        if(view.getId() == R.id.order_take_order_button){
            acceptOrder(mOrders.get(position).getId(), position);
        }
        if(view.getId() == R.id.restaurant_call_button){
            onCall("+" + mOrders.get(position).getOrderContact().getPartnerPhone());
        }
        if(view.getId() == R.id.client_call_button){
            onCall("+" + mOrders.get(position).getOrderContact().getClientPhone());
        }
    }

    public void onCall(String phone){
        int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE},
                    1);
        } else {
            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + phone)));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},1);
                }
                break;
            default:
                break;
        }
    }

    public void acceptOrder(int id, int position){
        api.acceptOrder(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPrefHelper.getToken(), Integer.toString(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        OrderStep step = response.body();
                        List<Object> status_list = new ArrayList<Object>();
                        status_list.add((Object)Double.parseDouble(step.getCurrent()));
                        status_list.add((Object)step.getDisplay());
                        mOrders.get(position).setStatus(status_list);
                        setupAdapter();
                    }else{
                        APIError errorBody = ErrorUtils.parseError(response, mRetrofit);
                    }
                });
    }

    public void restaurantOrder(int id, int position){
        api.restaurantOrder(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPrefHelper.getToken(), Integer.toString(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        OrderStep step = response.body();
                        List<Object> status_list = new ArrayList<Object>();
                        status_list.add((Object)Double.parseDouble(step.getCurrent()));
                        status_list.add((Object)step.getDisplay());
                        mOrders.get(position).setStatus(status_list);
                        setupAdapter();
                    }else{
                        APIError errorBody = ErrorUtils.parseError(response, mRetrofit);
                    }
                });
    }

    public void restaurantOutOrder(int id, int position){
        api.restaurantOutOrder(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPrefHelper.getToken(), Integer.toString(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        OrderStep step = response.body();
                        List<Object> status_list = new ArrayList<Object>();
                        status_list.add((Object)Double.parseDouble(step.getCurrent()));
                        status_list.add((Object)step.getDisplay());
                        mOrders.get(position).setStatus(status_list);
                        setupAdapter();
                    }else{
                        APIError errorBody = ErrorUtils.parseError(response, mRetrofit);
                    }
                });
    }

    public void clientOrder(int id, int position){
        api.clientOrder(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPrefHelper.getToken(), Integer.toString(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        OrderStep step = response.body();
                        List<Object> status_list = new ArrayList<Object>();
                        status_list.add((Object)Double.parseDouble(step.getCurrent()));
                        status_list.add((Object)step.getDisplay());
                        mOrders.get(position).setStatus(status_list);
                        setupAdapter();
                    }else{
                        APIError errorBody = ErrorUtils.parseError(response, mRetrofit);
                    }
                });
    }

    public void deliveredOrder(int id, int position){
        api.deliveredOrder(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPrefHelper.getToken(), Integer.toString(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        OrderStep step = response.body();
                        List<Object> status_list = new ArrayList<Object>();
                        status_list.add((Object)Double.parseDouble(step.getCurrent()));
                        status_list.add((Object)step.getDisplay());
                        mOrders.get(position).setStatus(status_list);
                        setupAdapter();
                    }else{
                        APIError errorBody = ErrorUtils.parseError(response, mRetrofit);
                    }
                });
    }

    public void getOrders(){
        api.getActiveOrders(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPrefHelper.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        List<ActiveOrder> orders = response.body();
                        mOrders = orders;
                        setupAdapter();
                    }
                    else{
                        //APIError error = ErrorUtils.parseError(response,mRetrofit);
                    }
                });
    }
}
