package kz.chocofood.chocofood_delivery.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.base.BaseFragment;



public class OrderDetailsFragment extends BaseFragment {


    @Override
    public String getFragmentTag() {
        return OrderDetailsFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_order_details;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.fragment_certain_order_details_title);
        toolbar.setBackgroundColor(getResources().getColor(R.color.choco_color_red));
        toolbar.setTitleTextColor(getResources().getColor(R.color.choco_color_white));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        getMainActivity().setSupportActionBar(toolbar);
        getMainActivity().getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getMainActivity().onBackPressed();
            }
        });
    }

    @Override
    protected boolean isDrawerDisabled() {
        return false;
    }

    public OrderDetailsFragment() {
        // Required empty public constructor
    }

    public static OrderDetailsFragment newInstance() {
        OrderDetailsFragment fragment = new OrderDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        return baseview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
