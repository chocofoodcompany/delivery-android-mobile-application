package kz.chocofood.chocofood_delivery.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.MainThread;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kz.chocofood.chocofood_delivery.MainApplication;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.ActiveOrdersActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.helpers.SharedPrefHelper;
import kz.chocofood.chocofood_delivery.helpers.TimerHelper;
import kz.chocofood.chocofood_delivery.models.User;
import kz.chocofood.chocofood_delivery.rest.API;
import kz.chocofood.chocofood_delivery.rest.APIError;
import kz.chocofood.chocofood_delivery.rest.ErrorUtils;
import kz.chocofood.chocofood_delivery.rest.RestConstants;
import retrofit2.Retrofit;


public class RiderStatusFragment extends BaseFragment implements View.OnClickListener{
    @BindView(R.id.timer) TextView mTimer;
    @BindView(R.id.fragment_rider_ready_button) Button mReady;
    @BindView(R.id.fragment_rider_deny_action) TextView mDeny;
    @BindView(R.id.fragment_rider_question) TextView mReadyStatus;
    @BindView(R.id.fragment_rider_time_left) TextView mTimeLeftText;

    @Inject Retrofit mRetrofit;
    @Inject SharedPrefHelper mSharedPreferences;
    @Inject API api;

    int duration=0;
    Bundle mSavedInstance;
    TimerHelper mTimerHelper = new TimerHelper();

    @Override
    public String getFragmentTag() {
        return RiderStatusFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_rider_status;
    }

    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
    }

    @Override
    protected boolean isDrawerDisabled() {
        return true;
    }

    public RiderStatusFragment() {
        // Required empty public constructor
    }

    public static RiderStatusFragment newInstance() {
        RiderStatusFragment fragment = new RiderStatusFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater,container,savedInstanceState);
        ButterKnife.bind(this,baseview);
        ((MainApplication)getActivity().getApplication()).getComponentNet().inject(this);
        mSavedInstance = savedInstanceState;
        mReady.setOnClickListener(this);
        getRiderStatus();
        return baseview;
    }


    public void initializeTimer(){
        new CountDownTimer(duration,1000){
            @Override
            public void onFinish() {
                mTimer.setText("00:00");
                mTimer.setTextColor(Color.RED);
            }

            @Override
            public void onTick(long l) {
                readyButtonCheck(l);
            }
        }.start();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }



    public void startTimer(Bundle savedInstanceState){
        if(savedInstanceState != null){
            if(mTimerHelper.restoreTimer(savedInstanceState.getString("time")) != 0){
                duration = mTimerHelper.restoreTimer(savedInstanceState.getString("time"));
            }
        }else{
            long miliseconds = Math.round(Double.parseDouble(mSharedPreferences.getCountdown()));
            duration = (int)miliseconds;
        }
        initializeTimer();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("time",mTimer.getText().toString());
    }

    public void readyButtonCheck(long l){
        if(l > 0){
            if(!mReady.isEnabled()){
                if(l <= 900000){
                    mReady.setEnabled(true);
                    mReady.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_rounded_green));
                }else{
                    mReady.setEnabled(false);
                    mReady.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_rounded_grey));
                }
            }
            mTimerHelper.convertElapsedTime(l);
            String yy = String.format("%02d:%02d:%02d", mTimerHelper.getHours(), mTimerHelper.getMinutes(),mTimerHelper.getSeconds());
            mTimer.setText(yy);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fragment_rider_ready_button:
                applyRiderStatus();
        }
    }

    private void prepareViews(){
        if(mSharedPreferences.getCanReady()){
            mReady.setEnabled(true);
            if(mSharedPreferences.getReady()){
                startActivity(ActiveOrdersActivity.newIntent(getContext()));
                getActivity().finish();
            }else{
                mReady.setEnabled(true);
                startTimer(mSavedInstance);
            }
        }
        else{
            mReadyStatus.setText(getResources().getString(R.string.fragment_rider_status_not_ready));
            mReady.setEnabled(false);
            mDeny.setEnabled(false);
            startTimer(mSavedInstance);
            //mTimer.getLayoutParams().height = 0;
            //mTimer.setVisibility(View.INVISIBLE);
            //mTimeLeftText.getLayoutParams().height = 0;
            //mTimeLeftText.setVisibility(View.INVISIBLE);
        }
    }

    private void getRiderStatus(){
        api.getUserStatus(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPreferences.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        User user = response.body();
                        if(user == null) return;
                        mSharedPreferences.saveUserStatusInformation(user);
                        prepareViews();
                    }
                    else{
                        APIError error = ErrorUtils.parseError(response,mRetrofit);
                    }
                }, throwable -> { });
    }

    private void applyRiderStatus(){
        api.applyUserStatus(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPreferences.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        mSharedPreferences.putIsWorkingNow(true);
                        startActivity(ActiveOrdersActivity.newIntent(getMainActivity()));
                    }
                    else{
                        APIError error = ErrorUtils.parseError(response,mRetrofit);
                    }
                }, throwable -> { });
    }

}
