package kz.chocofood.chocofood_delivery.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.LoginActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;

public class StartFragment extends BaseFragment {

    Boolean allowTransaction = false;
    @Override
    public String getFragmentTag() {
        return StartFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_start;
    }

    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
    }

    @Override
    protected boolean isDrawerDisabled() {
        return true;
    }

    public StartFragment() {
        // Required empty public constructor
    }

    public static StartFragment newInstance() {
        StartFragment fragment = new StartFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public Boolean isAllowTransaction(){
        return allowTransaction;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        allowTransaction = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        allowTransaction = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        allowTransaction = false;
    }

    @Override
    public void onStart() {
        super.onStart();
        allowTransaction = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater,container,savedInstanceState);
        allowTransaction = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getContext() != null){
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        }, 2000);
        return baseview;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
