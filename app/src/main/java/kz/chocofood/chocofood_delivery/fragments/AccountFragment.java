package kz.chocofood.chocofood_delivery.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;

public class AccountFragment extends BaseFragment {

    @Override
    public String getFragmentTag() {
        return AccountFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_account;
    }

    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.fragment_account_title));
        toolbar.setBackgroundColor(getResources().getColor(R.color.choco_color_red));
        toolbar.setTitleTextColor(getResources().getColor(R.color.choco_color_white));
        getMainActivity().setSupportActionBar(toolbar);
        getMainActivity().getSupportActionBar().setDisplayShowHomeEnabled(true);
        getMainActivity().setupToggleListener();
        getMainActivity().getNavigation_view().getMenu().getItem(BaseActivity.MENU_ITEMS.DRAWER_ID_ACCOUNT).setChecked(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getMainActivity().openDrawer();
            }
        });
    }

    @Override
    protected boolean isDrawerDisabled() {
        return false;
    }

    public AccountFragment() {
        // Required empty public constructor
    }

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        return baseview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
