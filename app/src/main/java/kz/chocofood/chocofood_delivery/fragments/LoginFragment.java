package kz.chocofood.chocofood_delivery.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kz.chocofood.chocofood_delivery.MainApplication;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.LoginActivity;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;
import kz.chocofood.chocofood_delivery.helpers.DialogHelper;
import kz.chocofood.chocofood_delivery.helpers.SharedPrefHelper;
import kz.chocofood.chocofood_delivery.models.User;
import kz.chocofood.chocofood_delivery.rest.API;
import kz.chocofood.chocofood_delivery.rest.APIError;
import kz.chocofood.chocofood_delivery.rest.ErrorUtils;
import kz.chocofood.chocofood_delivery.rest.RestConstants;
import kz.chocofood.chocofood_delivery.service.MyFirebaseInstanceID;
import kz.chocofood.chocofood_delivery.service.MyFirebaseMessagingService;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Query;

public class LoginFragment extends BaseFragment implements View.OnClickListener{
    @BindView(R.id.enter) Button enter;
    @BindView(R.id.fragment_login_field) EditText login_field;
    @BindView(R.id.fragment_login_password) EditText password_field;
    @BindView(R.id.fragment_container) RelativeLayout page_layout;

    @Inject API api;
    @Inject Retrofit mRetrofit;
    @Inject SharedPrefHelper mSharedPreferences;

    DialogHelper mDialogHelper;
    @Override
    public String getFragmentTag() {
        return LoginFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        if(isDrawerDisabled()){
            disableDrawer();
        }
    }

    @Override
    protected boolean isDrawerDisabled() {
        return true;
    }

    public LoginFragment() {
        // Required empty public constructor
    }


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater,container,savedInstanceState);
        ButterKnife.bind(this,baseview);
        ((MainApplication)getActivity().getApplication()).getComponentNet().inject(this);
        mDialogHelper = new DialogHelper();
        enter.setOnClickListener(this);
        if(!mSharedPreferences.getToken().equals("")){
            fragmentTransaction();
        }
        return baseview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.enter:
                mDialogHelper.showProgressDialog(getMainActivity());
                mDialogHelper.getProgressDialog().setMessage(getResources().getString(R.string.fragment_login_dialog_message));
                loginCommand(login_field.getText().toString(), password_field.getText().toString());
        }
    }

    private void fragmentTransaction(){
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.from_right,R.anim.to_left)
                .replace(R.id.fragment_container, new RiderStatusFragment(), RiderStatusFragment.class.getName())
                .addToBackStack(null)
                .commit();
    }

    public void updateSharedInfo(User user){
        mSharedPreferences.putToken(user.getToken());
        mSharedPreferences.putUsername(user.getUsername());
        mSharedPreferences.putPassword(user.getPassword());
    }

    private void sendFirebaseToken(){
        String firebase_token = FirebaseInstanceId.getInstance().getToken();
        HashMap<String, String> map = new HashMap<>();
        map.put("device_id", firebase_token);
        api.sendFirebaseToken(RestConstants.HEADER_AUTH_TOKEN_CONSTANT + mSharedPreferences.getToken(), firebase_token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if(response.isSuccessful()){
                    }
                    else{
                        APIError error = ErrorUtils.parseError(response,mRetrofit);
                    }
                }, throwable -> { throwable.toString(); });
    }

    private void loginCommand(String username, String password){
        HashMap<String, String> map = new HashMap<>();
        map.put("username", username);
        map.put("password", password);
                api.getUserToken(map)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(response -> {
                                 if(response.isSuccessful()){
                                        User user = response.body();
                                        if(user == null) return;
                                        updateSharedInfo(user);
                                        sendFirebaseToken();
                                        fragmentTransaction();
                                        mDialogHelper.hideProgressDialog();
                                    } else {
                                        APIError error = ErrorUtils.parseError(response,mRetrofit);
                                        if(error.getNon_field_errors() == null){
                                            if(error.getUsername() != null){
                                                login_field.setError(error.getUsername().get(0));
                                            }
                                            if(error.getPassword() != null){
                                                password_field.setError(error.getPassword().get(0));
                                            }
                                        }
                                        else{
                                            Snackbar.make(page_layout,
                                                    error.getNon_field_errors().get(0),
                                                    Snackbar.LENGTH_LONG).show();
                                        }
                                     mDialogHelper.hideProgressDialog();
                                    }
                            }, throwable -> { mDialogHelper.hideProgressDialog();});
            }

}
