package kz.chocofood.chocofood_delivery.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.adapters.ScheduleAdapter;
import kz.chocofood.chocofood_delivery.base.BaseActivity;
import kz.chocofood.chocofood_delivery.base.BaseFragment;

public class ScheduleFragment extends BaseFragment {

    @BindView(R.id.schedule_list) RecyclerView mScheduleList;
    ScheduleAdapter mScheduleAdapter;
    LinearLayoutManager mScheduleManager;
    @Override
    public String getFragmentTag() {
        return ScheduleFragment.class.getName();
    }

    @Override
    protected int onLayoutId() {
        return R.layout.fragment_schedule;
    }

    @Override
    protected void customizeActionBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.fragment_schedule_title));
        toolbar.setBackgroundColor(getResources().getColor(R.color.choco_color_red));
        toolbar.setTitleTextColor(getResources().getColor(R.color.choco_color_white));
        getMainActivity().setSupportActionBar(toolbar);
        getMainActivity().getSupportActionBar().setDisplayShowHomeEnabled(true);
        getMainActivity().setupToggleListener();
        getMainActivity().getNavigation_view().getMenu().getItem(BaseActivity.MENU_ITEMS.DRAWER_ID_SCHEDULE).setChecked(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getMainActivity().openDrawer();
            }
        });
    }

    @Override
    protected boolean isDrawerDisabled() {
        return false;
    }

    public ScheduleFragment() {
        // Required empty public constructor
    }

    public static ScheduleFragment newInstance() {
        ScheduleFragment fragment = new ScheduleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, baseview);
        setupAdapter();
        return baseview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setupAdapter(){
        ArrayList<String> mArray = new ArrayList<String>();
        mArray.add("1");
        mArray.add("1");
        mArray.add("1");
        mScheduleManager = new LinearLayoutManager(getContext());
        mScheduleList.setLayoutManager(mScheduleManager);
        mScheduleAdapter = new ScheduleAdapter(getContext(),mArray);
        mScheduleList.setAdapter(mScheduleAdapter);
    }
}
