package kz.chocofood.chocofood_delivery.helpers;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

/**
 * Created by User on 15.08.2017.
 */

public class TimerHelper {
    private int duration = 0;
    private long elapsedHours;
    private long elapsedMinutes;
    private long elapsedSeconds;

    public void convertElapsedTime(long millisUntilFinished){
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        this.elapsedHours = millisUntilFinished / hoursInMilli;
        millisUntilFinished = millisUntilFinished % hoursInMilli;

        this.elapsedMinutes = millisUntilFinished / minutesInMilli;
        millisUntilFinished = millisUntilFinished % minutesInMilli;

        this.elapsedSeconds = millisUntilFinished / secondsInMilli;
    }

    public long getHours(){
        return elapsedHours;
    }

    public long getMinutes(){
        return elapsedMinutes;
    }

    public long getSeconds(){
        return elapsedSeconds;
    }

    public int restoreTimer(String time){
        String[] separated = time.split(":");
        int hours = Integer.parseInt(separated[0]);
        int minutes = Integer.parseInt(separated[1]);
        int seconds = Integer.parseInt(separated[2]);
        long milisec = TimeUnit.MINUTES.toMillis(minutes);
        long secondss = TimeUnit.SECONDS.toMillis(seconds);
        long hourss = TimeUnit.HOURS.toMillis(hours);
        int total = (int)(long)(milisec + secondss + hourss);
        int endTime;
        if(hours != 0 && minutes != 0 && seconds != 0){
            endTime = hours*minutes*seconds *1000;
            duration = endTime;
        }
        if(hours == 0 && minutes != 0 && seconds != 0){
            endTime = minutes*seconds *1000;
            duration = endTime;
        }
        if(seconds != 0 && minutes ==0 && hours == 0){
            endTime = seconds *1000;
            duration = endTime;
        }
        duration = total;
        return duration;
    }
}
