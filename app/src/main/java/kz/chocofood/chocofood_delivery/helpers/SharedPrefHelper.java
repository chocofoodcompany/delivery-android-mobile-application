package kz.chocofood.chocofood_delivery.helpers;

import android.content.SharedPreferences;

import javax.inject.Inject;

import kz.chocofood.chocofood_delivery.models.User;

/**
 * Created by User on 31.08.2017.
 */

public class SharedPrefHelper {

    private SharedPreferences mSharedPreferences;

    @Inject
    public SharedPrefHelper(SharedPreferences sharedPreferences){
        this.mSharedPreferences = sharedPreferences;
    }

    public void putToken(String token){
        mSharedPreferences.edit().putString("token", token).apply();
    }

    public String getToken(){
        return mSharedPreferences.getString("token", "");
    }

    public void putUsername(String username){
        mSharedPreferences.edit().putString("username", username).apply();
    }

    public String getUsername(){
        return mSharedPreferences.getString("username", "");
    }

    public void putPassword(String password){
        mSharedPreferences.edit().putString("password", password).apply();
    }

    public String getPassword(){
        return mSharedPreferences.getString("password", "");
    }

    public void putCountDown(String countdown){
        mSharedPreferences.edit().putString("countdown", countdown).apply();
    }

    public String getCountdown(){
        return mSharedPreferences.getString("countdown", "");
    }

    public void putIsWorkingNow(Boolean is_working_now){
        mSharedPreferences.edit().putBoolean("is_working_now", is_working_now).apply();
    }

    public Boolean getWorkingNow(){
        return mSharedPreferences.getBoolean("is_working_now", false);
    }

    public void putIsReady(Boolean is_ready){
        mSharedPreferences.edit().putBoolean("is_ready", is_ready).apply();
    }

    public Boolean getReady(){
        return mSharedPreferences.getBoolean("is_ready", false);
    }

    public void putCanReady(Boolean can_ready){
        mSharedPreferences.edit().putBoolean("can_ready", can_ready).apply();
    }

    public Boolean getCanReady(){
        return mSharedPreferences.getBoolean("can_ready", false);
    }

    public void saveUserStatusInformation(User user){
        putCountDown(user.getCountdown());
        putCanReady(user.getCan_ready());
        putIsWorkingNow(user.getIs_working());
        putIsReady(user.getIs_ready());
    }

    public void clearUserData(){
        putCountDown("");
        putToken("");
        putCanReady(false);
        putIsReady(false);
        putIsWorkingNow(false);
        putUsername("");
        putPassword("");
    }

}
