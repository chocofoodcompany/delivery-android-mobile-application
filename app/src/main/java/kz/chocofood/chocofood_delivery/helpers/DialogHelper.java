package kz.chocofood.chocofood_delivery.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.provider.Settings;

import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.LoginActivity;

/**
 * Created by User on 31.08.2017.
 */

public class DialogHelper {
    ProgressDialog mProgressDialog;
    AlertDialog mNetworkDialog;

    public DialogHelper(){};

    public ProgressDialog getProgressDialog(){
        return mProgressDialog;
    }

    public void showProgressDialog(Activity activity){
        mProgressDialog = new ProgressDialog(activity, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    public void hideProgressDialog(){
        getProgressDialog().dismiss();
    }

    public AlertDialog.Builder buildNetworkDialog(Activity activity){
        return new AlertDialog.Builder(activity, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                .setMessage(R.string.dialog_network_message)
                .setPositiveButton(R.string.dialog_network_settings_button, (dialog, which) -> activity.startActivity(new Intent(Settings.ACTION_SETTINGS)))
                .setNegativeButton(R.string.dialog_network_cancel_button, ((dialog, i) -> dialog.cancel()));
    }

    public void showNetworkDialog(Activity activity){
        AlertDialog networkAlert = buildNetworkDialog(activity).create();
        networkAlert.show();
    }
}
