package kz.chocofood.chocofood_delivery.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.channguyen.rsv.RangeSliderView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.ActiveOrdersActivity;
import kz.chocofood.chocofood_delivery.fragments.RiderStatusFragment;
import kz.chocofood.chocofood_delivery.interfaces.IActiveOrdersInterface;
import kz.chocofood.chocofood_delivery.models.ActiveOrder;

/**
 * Created by User on 21.08.2017.
 */

public class ActiveOrdersAdapter extends RecyclerView.Adapter<ActiveOrdersAdapter.ActiveOrdersHolder>{
    private Context mContext;
    private List<ActiveOrder> mOrders;
    private IActiveOrdersInterface listener;

    public ActiveOrdersAdapter(Context context, List<ActiveOrder> orders, IActiveOrdersInterface listener){
        this.mContext = context;
        this.mOrders = orders;
        this.listener = listener;
    }

    @Override
    public ActiveOrdersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.l_item_active_order,null);
        return new ActiveOrdersHolder(view);
    }

    @Override
    public void onBindViewHolder(final ActiveOrdersHolder holder, int position) {

        Log.d("ActiveOrdersAdapter", mOrders.get(position).getOrderInfo().getDatetimePartner());
        Log.d("ActiveOrdersAdapter", mOrders.get(position).getOrderInfo().getDatetimeClient());
        holder.mOrderId.setText(mContext.getString(R.string.l_item_order_model_client_id, mOrders.get(position).getOrder().getClientOrderCode()));
        holder.mOrderPrice.setText(mContext.getString(R.string.l_item_order_model_order_price,mOrders.get(position).getOrder().getAmountFromClient()));

        holder.mRestAddressName.setText(mOrders.get(position).getOrderContact().getPartnerName());
        holder.mRestAddressValue.setText(mOrders.get(position).getOrder().getAddressFrom());
        holder.mRestOrderPrice.setText(mContext.getString(R.string.l_item_order_model_pay_to_restaurant, mOrders.get(position).getOrder().getAmountToPartner()));
        //holder.mRestAddressTime.setText(convertTime(mOrders.get(position).getOrderInfo().getDatetimePartner()));
        holder.mRestAddressDistValue.setText("?????");
        holder.mClientName.setText(mOrders.get(position).getOrderContact().getClientName());
        holder.mClientAddress.setText(mOrders.get(position).getOrder().getAddressTo());
        holder.mOrderDeliveryPrice.setText(mContext.getString(R.string.l_item_order_model_take_from_client,mOrders.get(position).getOrder().getAmountFromClient()));
        //holder.mClientDeliveryTime.setText(convertTime(mOrders.get(position).getOrderInfo().getDatetimeClient()));
        holder.mClientAddressDist.setText("?????");

        Double d = (Double)mOrders.get(position).getStatus().get(0);

        if(Double.toString(d.intValue()).equals("200.0")){
            holder.step_slider.setFilledColor(mContext.getResources().getColor(R.color.choco_color_light_grey));

            holder.mOrderMakeStep.getLayoutParams().height = 0;
            holder.mOrderPayStatus.setText(R.string.l_item_order_model_status_new_text);
            holder.mOrderPayStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_rounded_blue));
            holder.mOrderPayStatus.setPadding(1,1,1,1);

            holder.mOrderRestaurantStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderTakenStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderArrivedStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderDeliveredStatusText.setVisibility(View.INVISIBLE);
        }
        if(Double.toString(d.intValue()).equals("201.0")){
            holder.step_slider.setFilledColor(mContext.getResources().getColor(R.color.choco_color_green));

            holder.mOrderAccept.getLayoutParams().height = 0;

            holder.mOrderPayStatus.setText("");
            holder.mOrderPayStatus.setBackgroundDrawable(null);

            holder.mOrderStatusTitleText.setVisibility(View.INVISIBLE);
            holder.mOrderRestaurantStatusText.setVisibility(View.VISIBLE);
            holder.mOrderTakenStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderArrivedStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderDeliveredStatusText.setVisibility(View.INVISIBLE);

            holder.mOrderAcceptStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));

            holder.mOrderMakeStep.setVisibility(View.VISIBLE);
            holder.mOrderMakeStep.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
        }
        if(Double.toString(d.intValue()).equals("300.0")){
            holder.step_slider.setFilledColor(mContext.getResources().getColor(R.color.choco_color_green));
            holder.step_slider.setInitialIndex(1);

            holder.mOrderAccept.getLayoutParams().height = 0;

            holder.mOrderPayStatus.setText("");
            holder.mOrderPayStatus.setBackgroundDrawable(null);

            holder.mOrderStatusTitleText.setVisibility(View.INVISIBLE);
            holder.mOrderRestaurantStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderTakenStatusText.setVisibility(View.VISIBLE);
            holder.mOrderArrivedStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderDeliveredStatusText.setVisibility(View.INVISIBLE);

            holder.mOrderAcceptStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderRestaurantStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));

            holder.mOrderMakeStep.setVisibility(View.VISIBLE);
            holder.mOrderMakeStep.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
        }
        if(Double.toString(d.intValue()).equals("301.0")){
            holder.step_slider.setFilledColor(mContext.getResources().getColor(R.color.choco_color_green));
            holder.step_slider.setInitialIndex(2);

            holder.mOrderAccept.getLayoutParams().height = 0;

            holder.mOrderPayStatus.setText("");
            holder.mOrderPayStatus.setBackgroundDrawable(null);

            holder.mOrderStatusTitleText.setVisibility(View.INVISIBLE);
            holder.mOrderRestaurantStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderTakenStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderArrivedStatusText.setVisibility(View.VISIBLE);
            holder.mOrderDeliveredStatusText.setVisibility(View.INVISIBLE);

            holder.mOrderAcceptStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderRestaurantStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderTakenStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));

            holder.mOrderMakeStep.setVisibility(View.VISIBLE);
            holder.mOrderMakeStep.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
        }
        if(Double.toString(d.intValue()).equals("400.0")){
            holder.step_slider.setFilledColor(mContext.getResources().getColor(R.color.choco_color_green));
            holder.step_slider.setInitialIndex(3);

            holder.mOrderAccept.getLayoutParams().height = 0;

            holder.mOrderPayStatus.setText("");
            holder.mOrderPayStatus.setBackgroundDrawable(null);

            holder.mOrderStatusTitleText.setVisibility(View.INVISIBLE);
            holder.mOrderRestaurantStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderTakenStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderArrivedStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderDeliveredStatusText.setVisibility(View.VISIBLE);

            holder.mOrderAcceptStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderRestaurantStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderTakenStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderArrivedStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));

            holder.mOrderMakeStep.setVisibility(View.VISIBLE);
            holder.mOrderMakeStep.setText(R.string.l_item_order_model_close_order);
            holder.mOrderMakeStep.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
        }
        if(Double.toString(d.intValue()).equals("401.0")){
            holder.step_slider.setFilledColor(mContext.getResources().getColor(R.color.choco_color_green));
            holder.step_slider.setInitialIndex(4);

            holder.mOrderAccept.getLayoutParams().height = 0;

            holder.mOrderPayStatus.setText("");
            holder.mOrderPayStatus.setBackgroundDrawable(null);

            holder.mOrderStatusTitleText.setVisibility(View.INVISIBLE);
            holder.mOrderRestaurantStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderTakenStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderArrivedStatusText.setVisibility(View.INVISIBLE);
            holder.mOrderDeliveredStatusText.setVisibility(View.INVISIBLE);

            holder.mOrderAcceptStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderRestaurantStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderTakenStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderArrivedStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));
            holder.mOrderDeliveredStatusImage.setColorFilter(new PorterDuffColorFilter(mContext.getResources().getColor(R.color.choco_color_green), PorterDuff.Mode.SRC_IN));

            holder.mOrderMakeStep.setVisibility(View.VISIBLE);
            holder.mOrderMakeStep.getLayoutParams().height = 0;
        }
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }

    class ActiveOrdersHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.order_id) TextView mOrderId;
        @BindView(R.id.order_price_value) TextView mOrderPrice;
        @BindView(R.id.order_payment_status) TextView mOrderPayStatus;

        @BindView(R.id.order_accept_status_image) ImageView mOrderAcceptStatusImage;
        @BindView(R.id.order_restaurant_status_image) ImageView mOrderRestaurantStatusImage;
        @BindView(R.id.order_taken_status_image) ImageView mOrderTakenStatusImage;
        @BindView(R.id.order_arrived_status_image) ImageView mOrderArrivedStatusImage;
        @BindView(R.id.order_delivered_status_image) ImageView mOrderDeliveredStatusImage;

        @BindView(R.id.order_step_slider) RangeSliderView step_slider;

        @BindView(R.id.order_status_title_text) TextView mOrderStatusTitleText;
        @BindView(R.id.order_restaurant_status_text) TextView mOrderRestaurantStatusText;
        @BindView(R.id.order_taken_status_text) TextView mOrderTakenStatusText;
        @BindView(R.id.order_arrived_status_text) TextView mOrderArrivedStatusText;
        @BindView(R.id.order_delivered_status_text) TextView mOrderDeliveredStatusText;

        @BindView(R.id.order_make_step_button) Button mOrderMakeStep;
        @BindView(R.id.order_take_order_button) Button mOrderAccept;
        @BindView(R.id.order_details_button) Button mOrderDetails;

        @BindView(R.id.restaurant_address_layout) RelativeLayout mRestLayout;
        @BindView(R.id.restaurant_name) TextView mRestAddressName;
        @BindView(R.id.restaurant_order_time) TextView mRestAddressTime;
        @BindView(R.id.restaurant_address_value) TextView mRestAddressValue;
        @BindView(R.id.restaurant_address_distance_value) TextView mRestAddressDistValue;
        @BindView(R.id.restaurant_order_delivery) TextView mRestOrderPrice;
        @BindView(R.id.restaurant_call_button) ImageView mRestCall;

        @BindView(R.id.client_address_layout) RelativeLayout mClientLayout;
        @BindView(R.id.client_name) TextView mClientName;
        @BindView(R.id.client_delivery_order_time) TextView mClientDeliveryTime;
        @BindView(R.id.client_address_value) TextView mClientAddress;
        @BindView(R.id.client_address_distance_value) TextView mClientAddressDist;
        @BindView(R.id.order_delivery_price) TextView mOrderDeliveryPrice;
        @BindView(R.id.client_call_button) ImageView mClientCall;
        ActiveOrdersHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            mOrderDetails.setOnClickListener(this);
            mOrderMakeStep.setOnClickListener(this);
            mOrderAccept.setOnClickListener(this);
            mRestCall.setOnClickListener(this);
            mClientCall.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onViewClicked(view,getAdapterPosition());
        }
    }
}
