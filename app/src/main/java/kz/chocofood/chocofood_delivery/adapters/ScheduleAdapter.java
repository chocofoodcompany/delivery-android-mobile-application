package kz.chocofood.chocofood_delivery.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.chocofood.chocofood_delivery.R;

/**
 * Created by User on 21.08.2017.
 */

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleHolder> {
    private ArrayList<String> mScheduleArray;
    private Context mContext;

    public ScheduleAdapter(Context context, ArrayList<String> scheduleArray){
        this.mContext = context;
        this.mScheduleArray = scheduleArray;
    }

    @Override
    public ScheduleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.l_item_schedule,null);
        return new ScheduleHolder(view);
    }

    @Override
    public void onBindViewHolder(ScheduleHolder holder, int position) {
        if(position == 1){
            holder.mScheduleLayout.setBackgroundColor(mContext.getResources().getColor(R.color.choco_color_light_grey));
            holder.mScheduleDay.setText("Завтра ВТОРНИК");
        }
        if(position == 2){
            holder.mScheduleLayout.setBackgroundColor(mContext.getResources().getColor(R.color.choco_color_light_grey));
            holder.mScheduleDay.setText("СРЕДА");
        }
    }

    @Override
    public int getItemCount() {
        return mScheduleArray.size();
    }

    class ScheduleHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.schedule_item_day_layout) LinearLayout mScheduleLayout;
        @BindView(R.id.schedule_item_day_value) TextView mScheduleDay;
        @BindView(R.id.schedule_item_time_value) TextView mScheduleTimeValue;
        @BindView(R.id.schedule_item_image) ImageView mScheduleImage;
        @BindView(R.id.schedule_item_time_point) TextView mScheduleTimePoint;
        @BindView(R.id.schedule_item_start_point) TextView mScheduleStartPoint;
        @BindView(R.id.schedule_item_point_value) TextView mSchedulePointValue;
        ScheduleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
