package kz.chocofood.chocofood_delivery.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.models.ArchiveOrder;

/**
 * Created by User on 21.08.2017.
 */

public class ArchiveOrdersAdapter extends RecyclerView.Adapter<ArchiveOrdersAdapter.ArchiveOrdersHolder> {
    private Context mContext;
    private ArrayList<ArchiveOrder> mArchiveOrders;

    public ArchiveOrdersAdapter(Context context, ArrayList<ArchiveOrder> mArchiveOrders){
        this.mContext = context;
        this.mArchiveOrders = mArchiveOrders;
    }

    @Override
    public ArchiveOrdersAdapter.ArchiveOrdersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.l_item_active_order,null);
        return new ArchiveOrdersHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ArchiveOrdersAdapter.ArchiveOrdersHolder holder, int position) {

        holder.mOrderPayStatus.setText(R.string.l_item_archive_order_model_status);

        holder.mOrderApproveLayout.getLayoutParams().height = 0;
        holder.mOrderApproveLayout.setVisibility(View.INVISIBLE);

        holder.mOrderAcceptLayout.getLayoutParams().height = 0;
        holder.mOrderAcceptLayout.setVisibility(View.INVISIBLE);

        holder.mStepLayout.getLayoutParams().height = 0;
        holder.mStepLayout.setVisibility(View.INVISIBLE);

        holder.mProgressTitles.getLayoutParams().height = 0;
        holder.mProgressTitles.setVisibility(View.INVISIBLE);

        holder.mOrderDetails.setBackgroundDrawable(null);
        holder.mOrderDetails.setText(R.string.l_item_archive_order_model_details);
        holder.mOrderDetails.setTextColor(mContext.getResources().getColor(R.color.choco_color_dark_grey));
        holder.mOrderDetails.setForeground(null);

    }

    @Override
    public int getItemCount() {
        return mArchiveOrders.size();
    }

    class ArchiveOrdersHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.order_approve_button) LinearLayout mOrderApproveLayout;
        @BindView(R.id.order_accept_button) LinearLayout mOrderAcceptLayout;
        @BindView(R.id.order_base_info_layout) LinearLayout mOrderBaseLayout;
        @BindView(R.id.step_base_layout) LinearLayout mStepLayout;
        @BindView(R.id.order_progress_titles) LinearLayout mProgressTitles;

        @BindView(R.id.order_id) TextView mOrderId;
        @BindView(R.id.order_price_value) TextView mOrderPrice;
        @BindView(R.id.order_payment_status) TextView mOrderPayStatus;

        @BindView(R.id.order_accept_status_image) ImageView mOrderAcceptStatusImage;
        @BindView(R.id.order_restaurant_status_image) ImageView mOrderRestaurantStatusImage;
        @BindView(R.id.order_taken_status_image) ImageView mOrderTakenStatusImage;
        @BindView(R.id.order_arrived_status_image) ImageView mOrderArrivedStatusImage;
        @BindView(R.id.order_delivered_status_image) ImageView mOrderDeliveredStatusImage;

        @BindView(R.id.order_status_title_text) TextView mOrderStatusTitleText;
        @BindView(R.id.order_restaurant_status_text) TextView mOrderRestaurantStatusText;
        @BindView(R.id.order_taken_status_text) TextView mOrderTakenStatusText;
        @BindView(R.id.order_arrived_status_text) TextView mOrderArrivedStatusText;
        @BindView(R.id.order_delivered_status_text) TextView mOrderDeliveredStatusText;

        @BindView(R.id.order_make_step_button) Button mOrderMakeStep;
        @BindView(R.id.order_take_order_button) Button mOrderAccept;
        @BindView(R.id.order_details_button) Button mOrderDetails;

        @BindView(R.id.restaurant_address_layout) RelativeLayout mRestLayout;
        @BindView(R.id.restaurant_name) TextView mRestAddressName;
        @BindView(R.id.restaurant_order_time) TextView mRestAddressTime;
        @BindView(R.id.restaurant_address_value) TextView mRestAddressValue;
        @BindView(R.id.restaurant_address_distance_value) TextView mRestAddressDistValue;
        @BindView(R.id.restaurant_order_delivery) TextView mRestOrderPrice;
        @BindView(R.id.restaurant_call_button) ImageView mRestCall;

        @BindView(R.id.client_address_layout) RelativeLayout mClientLayout;
        @BindView(R.id.client_name) TextView mClientName;
        @BindView(R.id.client_delivery_order_time) TextView mClientDeliveryTime;
        @BindView(R.id.client_address_value) TextView mClientAddress;
        @BindView(R.id.client_address_distance_value) TextView mClientAddressDist;
        @BindView(R.id.order_delivery_price) TextView mOrderDeliveryPrice;
        @BindView(R.id.client_call_button) ImageView mClientCall;
        ArchiveOrdersHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
