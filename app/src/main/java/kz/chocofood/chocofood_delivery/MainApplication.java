package kz.chocofood.chocofood_delivery;

import android.app.Application;

import dagger.Component;
import dagger.internal.DaggerCollections;
import kz.chocofood.chocofood_delivery.di.component.ComponentNet;
import kz.chocofood.chocofood_delivery.di.component.DaggerComponentNet;
import kz.chocofood.chocofood_delivery.di.modules.ModuleApp;
import kz.chocofood.chocofood_delivery.di.modules.ModuleNet;

/**
 * Created by User on 28.08.2017.
 */

public class MainApplication extends Application {
    //region Vars
    private ComponentNet componentNet;
    //endregion

    @Override
    public void onCreate() {
        super.onCreate();

        //region Dagger
        componentNet = DaggerComponentNet
                .builder()
                .moduleApp(new ModuleApp(this))
                .moduleNet(new ModuleNet())
                .build();
        componentNet.inject(this);
        //endregion
    }

    public ComponentNet getComponentNet() {
        return componentNet;
    }
}
