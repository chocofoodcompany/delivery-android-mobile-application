package kz.chocofood.chocofood_delivery.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import kz.chocofood.chocofood_delivery.R;
import kz.chocofood.chocofood_delivery.activity.LoginActivity;

/**
 * Created by User on 29.08.2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if(remoteMessage.getFrom() != null){
            sendOrderNotification(getResources().getString(R.string.firebase_notification_message));
        }

        if (remoteMessage.getNotification() != null) {
            sendOrderNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void sendOrderNotification(String messageBody) {
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_alert)
                .setContentText(getString(R.string.firebase_notification_message))
                .setContentTitle(getString(R.string.firebase_notification_title))
                .setSound(uri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
